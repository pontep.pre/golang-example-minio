package logs

import (
	"os"
	"runtime"
	"strconv"
	"strings"

	"github.com/sirupsen/logrus"
)

type Logrus interface {
	//InitLogs()
	LogEntry() *logrus.Entry
	Debug(args ...interface{})
	Debugf(template string, args ...interface{})
	Info(args ...interface{})
	Infof(template string, args ...interface{})
	Warn(args ...interface{})
	Warnf(template string, args ...interface{})
	Error(args ...interface{})
	Errorf(template string, args ...interface{})
	Fatal(args ...interface{})
	Fatalf(template string, args ...interface{})
}

type logs struct {
	logrus *logrus.Logger
}

func NewLogrus() Logrus {
	logger := logrus.New()
	logger.SetFormatter(&logrus.JSONFormatter{})
	logger.SetOutput(os.Stdout)
	logger.SetLevel(logrus.DebugLevel)
	return &logs{
		logrus: logger,
	}
}

func (l *logs) LogEntry() *logrus.Entry {
	pc, file, line, ok := runtime.Caller(1)
	if !ok {
		l.logrus.Error("Could not get context info for logger!")
	}

	filename := file[strings.LastIndex(file, "/")+1:] + ":" + strconv.Itoa(line)
	funcname := runtime.FuncForPC(pc).Name()
	fn := funcname[strings.LastIndex(funcname, ".")+1:]
	return l.logrus.WithField("file", filename).WithField("function", fn)
}

func (l *logs) Debug(args ...interface{}) {
	l.logrus.Debug(args...)
}

func (l *logs) Debugf(template string, args ...interface{}) {
	l.logrus.Debugf(template, args...)
}

func (l *logs) Info(args ...interface{}) {
	l.logrus.Info(args...)
}

func (l *logs) Infof(template string, args ...interface{}) {
	l.logrus.Infof(template, args...)
}

func (l *logs) Warn(args ...interface{}) {
	l.logrus.Warn(args...)
}

func (l *logs) Warnf(template string, args ...interface{}) {
	l.logrus.Warnf(template, args...)
}

func (l *logs) Error(args ...interface{}) {
	l.logrus.Error(args...)
}

func (l *logs) Errorf(template string, args ...interface{}) {
	l.logrus.Errorf(template, args...)
}

func (l *logs) Panic(args ...interface{}) {
	l.logrus.Panic(args...)
}

func (l *logs) Panicf(template string, args ...interface{}) {
	l.logrus.Panicf(template, args...)
}

func (l *logs) Fatal(args ...interface{}) {
	l.logrus.Fatal(args...)
}

func (l *logs) Fatalf(template string, args ...interface{}) {
	l.logrus.Fatalf(template, args...)
}
