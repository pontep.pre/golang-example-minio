package storages

import (
	"context"
	"log"
	"net/url"
	"os"
	"time"

	"github.com/minio/minio-go/v7"
	"github.com/minio/minio-go/v7/pkg/credentials"
)

var minioClient *minio.Client
var bucket = os.Getenv("MINIO_BUCKET")

// MinIO ...
type MinIO struct {
}

func NewMinIO() (*minio.Client, error) {
	endpoint := os.Getenv("MINIO_HOST")
	accessKeyID := os.Getenv("MINIO_KEY_ID")
	secretAccessKey := os.Getenv("MINIO_ACCESS_KEY")
	useSSL := true

	// Initialize minio client object.
	client, err := minio.New(endpoint, &minio.Options{
		Creds:  credentials.NewStaticV4(accessKeyID, secretAccessKey, ""),
		Secure: useSSL,
	})

	if err != nil {
		return nil, err
	}
	minioClient = client
	return minioClient, err
}

func (m *MinIO) GetBucketName() string {
	return bucket
}

func (m *MinIO) MakeBucket() error {
	ctx := context.Background()
	err := minioClient.MakeBucket(ctx, bucket, minio.MakeBucketOptions{Region: "us-east-1"})
	if err != nil {
		// Check to see if we already own this bucket (which happens if you run this twice)
		exists, errBucketExists := minioClient.BucketExists(ctx, bucket)
		if !(errBucketExists == nil && exists) {
			return err
		}
	} else {
		log.Printf("Successfully created bucket: %s\n", bucket)
	}
	return nil
}

func (m *MinIO) PutObject(file_path string, file_type string, object string) (string, error) {
	var key string

	file, err := os.Open(file_path)
	if err != nil {
		return key, err
	}
	defer file.Close()

	fileStat, err := file.Stat()
	if err != nil {
		return key, err
	}

	UploadInfo, err := minioClient.PutObject(context.Background(), bucket, object, file, fileStat.Size(), minio.PutObjectOptions{ContentType: file_type})
	if err != nil {
		return key, err
	}

	return UploadInfo.Key, err
}

func (m *MinIO) PresignedGetObject(bucket string, object string) (string, error) {
	// Set request parameters for content-disposition.
	reqParams := make(url.Values)

	// // Generates a presigned url which expires in a day.
	presignedURL, err := minioClient.PresignedGetObject(context.Background(), bucket, object, time.Second*24*60*60, reqParams)
	if err != nil {
		return "", err
	}
	return presignedURL.String(), err
}
