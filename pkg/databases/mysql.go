package databases

import (
	"context"
	"database/sql"
	"time"

	_ "github.com/go-sql-driver/mysql"
)

var DB *sql.DB

type Mysql struct {
	sessionDB *sql.DB
}

func NewMysql(cfg string) (*sql.DB, error) {
	var client Mysql
	//connect mysql
	db, err := sql.Open("mysql", cfg)
	if err != nil {
		return nil, err
	}
	client.sessionDB = db
	DB = db
	return db, err
}

func HelpCheckDB(db *sql.DB) error {
	//ping check database
	ctx, cancelfunc := context.WithTimeout(context.Background(), 5*time.Second)
	defer cancelfunc()
	err := db.PingContext(ctx)
	if err != nil {
		return err
	}
	return nil
}
