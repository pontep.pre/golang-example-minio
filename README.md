# api-to-logsfile

## Environment Variable
`API_VERSION` version ของ API

`DB_HOST` address สำหรับเชื่อต่อฐานข้อมูล

`MAX_UPLOAD_SIZE` ขนาดไฟล์ที่สามารถอัปโหลดได้ (MB)

`EXPIRED_DATE_LOG` อายุของไฟล์ log

`MINIO_HOST` minio host ที่เก็บไฟล์ backup

`MINIO_KEY_ID` key_id ของ minio

`MINIO_ACCESS_KEY` access_key ของ minio

`MINIO_BUCKET` ชื่อ bucket ของ minio

## Install prerequisites
ก่อนที่จะติดตั้งระบบให้ตรวจสอบให้แน่ใจว่าได้ทำการติดตั้งซอฟต์แวร์ ดังนี้
* [Git](https://git-scm.com/downloads)
* [Docker](https://www.docker.com/get-started)

## Getting Started
**1. ดาวน์โหลด source code ของระบบ**
```sh
git clone https://gitlab.spinsoft.co.th/d1669/backend/api-to-logsfile
```
**2. เปิดโฟลเดอร์ของระบบที่ได้ clone ลงมาเรียบร้อยแล้ว**
```sh
cd api-to-logsfile
```

**3. Build docker**
```sh
docker build -t api-to-logsfile .
```
**4. Run docker**
```sh
docker run --env-file .env api-to-logsfile
```

## Endpoints 

**POST** {{url}}/v1/logs
- API สำหรับอัปโหลดไฟล์

**GET** {{url}}/v1/logs/:id/presign-url
- API สำหรับ generate ลิงก์ของไฟล์

**รายละเอียดเพิ่มเติมสามารถดูได้ที่นี่ [Postman Document](https://www.getpostman.com/collections/b5377a14493425374a46)**