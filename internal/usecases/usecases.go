package usecases

import (
	"app/internal/models"
	"mime/multipart"
)

type Service interface {
	//file
	UploadFile(file multipart.File, file_name string, path string) (string, error)
	RemoveDir(dir string) error
	GetFileType(file multipart.File) (string, error)
	GetAssetFile(path string) string
	//log file
	DeleteLogFile() error
	BackupLogFile() error
	GetPresignUrl(metadata models.Metadata) (string, error)
	//metadata
	InsertMetadata(metadata models.Metadata) error
	GetMetadata(filters models.FilterMetadata) ([]models.Metadata, error)
	GetMetadataByDetail(metadata models.Metadata) (models.Metadata, error)
	GetMetadataByID(id string) (models.Metadata, error)
	UpdateMetadata(metadata models.Metadata) error
	UpdateMetadataIsBackUp(metadata models.Metadata) error
	//minio_storage
	InsertMinIOStorage(storage models.MinIOStorage) error
}

type Repository interface {
	//metadata
	InsertMetadata(metadata models.Metadata) error
	UpdateMetadata(metadata models.Metadata) error
	GetMetadata(filters models.FilterMetadata) ([]models.Metadata, error)
	GetMetadataByDetail(metadata models.Metadata) (models.Metadata, error)
	GetMetadataByID(id string) (models.Metadata, error)
	UpdateMetadataIsBackUp(metadata models.Metadata) error
	UpdateMetadataIsDelete(metadata models.Metadata) error
	//minio_storage
	InsertMinIOStorage(storage models.MinIOStorage) error
	GetMinIOStorage(id string) (models.MinIOStorage, error)
}
