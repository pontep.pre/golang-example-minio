package services

import (
	"io"
	"mime/multipart"
	"net/http"
	"os"
	"regexp"
)

func (s *service) GetAssetFile(path string) string {
	var asset string
	reg := regexp.MustCompile(`uploads/`)
	asset = reg.ReplaceAllString(path, ("/files/"))
	//asset = os.Getenv("URL_HOST") + asset
	return asset
}

func (s *service) UploadFile(file multipart.File, file_name string, path string) (string, error) {

	var asset string
	var handleErr error
	dir := "uploads/" + path

	_, handleErr = file.Seek(0, io.SeekStart)
	if handleErr != nil {
		return asset, handleErr
	}

	// Create the uploads folder if it doesn't
	// already exist
	handleErr = os.MkdirAll(dir, 0755)
	if handleErr != nil {
		return asset, handleErr
	}

	// Create a new file in the uploads directory
	file_path := dir + file_name
	new_file, handleErr := os.Create(file_path)
	defer new_file.Close()
	if handleErr != nil {
		return asset, handleErr
	}

	// Copy the uploaded file to the filesystem
	// at the specified destination
	_, handleErr = io.Copy(new_file, file)
	if handleErr != nil {
		return asset, handleErr
	}
	asset = file_path
	return asset, nil
}

func (s *service) GetFileType(file multipart.File) (string, error) {
	var file_type string
	var handleErr error
	buff := make([]byte, 512)
	_, handleErr = file.Read(buff)
	if handleErr != nil {
		return file_type, handleErr
	}
	file_type = http.DetectContentType(buff)
	return file_type, handleErr
}

func (s *service) RemoveDir(dir string) error {
	err := os.RemoveAll(dir)
	if err != nil {
		return err
	}
	return nil
}

func createDir(dir string) error {
	err := os.MkdirAll(dir, 0755)
	if err != nil {
		return err
	}
	return nil
}

func removeDir(dir string) error {
	err := os.RemoveAll(dir)
	if err != nil {
		return err
	}
	return nil
}

func exists(dir string) (bool, error) {
	_, err := os.Stat(dir)
	if err == nil {
		return true, nil
	}
	if os.IsNotExist(err) {
		return false, nil
	}
	return false, err
}
