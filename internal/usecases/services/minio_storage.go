package services

import "app/internal/models"

func (s *service) InsertMinIOStorage(storage models.MinIOStorage) error {
	var handleErr error
	handleErr = s.repository.InsertMinIOStorage(storage)
	if handleErr != nil {
		return handleErr
	}
	return handleErr
}
