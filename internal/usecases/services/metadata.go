package services

import "app/internal/models"

func (s *service) GetMetadata(filters models.FilterMetadata) ([]models.Metadata, error) {
	var metadata []models.Metadata
	var handleErr error
	metadata, handleErr = s.repository.GetMetadata(filters)
	if handleErr != nil {
		return metadata, handleErr
	}
	return metadata, handleErr
}

func (s *service) GetMetadataByDetail(metadata models.Metadata) (models.Metadata, error) {
	var data models.Metadata
	var handleErr error
	data, handleErr = s.repository.GetMetadataByDetail(metadata)
	if handleErr != nil {
		return data, handleErr
	}
	return data, handleErr
}

func (s *service) GetMetadataByID(id string) (models.Metadata, error) {
	var data models.Metadata
	var handleErr error
	data, handleErr = s.repository.GetMetadataByID(id)
	if handleErr != nil {
		return data, handleErr
	}
	return data, handleErr
}

func (s *service) InsertMetadata(metadata models.Metadata) error {
	var handleErr error
	handleErr = s.repository.InsertMetadata(metadata)
	if handleErr != nil {
		return handleErr
	}
	return handleErr
}

func (s *service) UpdateMetadata(metadata models.Metadata) error {
	var handleErr error
	handleErr = s.repository.UpdateMetadata(metadata)
	if handleErr != nil {
		return handleErr
	}
	return handleErr
}

func (s *service) UpdateMetadataIsBackUp(metadata models.Metadata) error {
	var handleErr error
	handleErr = s.repository.UpdateMetadataIsBackUp(metadata)
	if handleErr != nil {
		return handleErr
	}
	return handleErr
}
