package services

import (
	"app/internal/usecases"
	"app/pkg/storages"
)

type service struct {
	repository usecases.Repository
	minio      *storages.MinIO
}

func NewService(repo usecases.Repository) usecases.Service {
	return &service{
		repository: repo,
	}
}
