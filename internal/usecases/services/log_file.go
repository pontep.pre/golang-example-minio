package services

import (
	"app/internal/models"
	"errors"
	"os"
	"regexp"
	"strconv"
	"time"
)

func (s *service) GetPresignUrl(metadata models.Metadata) (string, error) {
	var data models.MinIOStorage
	var handleErr error
	var url string

	data, handleErr = s.repository.GetMinIOStorage(metadata.ID)
	if handleErr != nil {
		return url, handleErr
	}

	url, handleErr = s.minio.PresignedGetObject(data.Bucket, data.Object)
	if handleErr != nil {
		return url, handleErr
	}

	return url, nil
}

func (s *service) DeleteLogFile() error {

	expired_date := getExpriredDate()

	var filters models.FilterMetadata
	filters.FileDate = expired_date
	filters.IsBackup = "1"

	var data []models.Metadata
	var handleErr error
	data, handleErr = s.repository.GetMetadata(filters)
	if handleErr != nil {
		return handleErr
	}

	unique := unique(data)

	var metadata models.Metadata
	for _, v := range unique {
		dir := "./uploads/" + v.BranchID + "/" + v.FileDate
		handleErr = s.RemoveDir(dir)
		if handleErr != nil {
			return handleErr
		}
		metadata.BranchID = v.BranchID
		metadata.FileDate = v.FileDate
		handleErr = s.repository.UpdateMetadataIsDelete(metadata)
		if handleErr != nil {
			return handleErr
		}
	}

	return handleErr
}

func (s *service) BackupLogFile() error {
	expired_date := getExpriredDate()

	var filters models.FilterMetadata
	filters.FileDate = expired_date
	filters.IsBackup = "0"

	var data []models.Metadata
	var handleErr error
	data, handleErr = s.repository.GetMetadata(filters)
	if handleErr != nil {
		return handleErr
	}

	if len(data) == 0 {
		return errors.New("backup file not found")
	}

	handleErr = s.minio.MakeBucket()
	if handleErr != nil {
		return handleErr
	}

	var storage models.MinIOStorage
	for _, v := range data {
		file_path := "./" + v.FilePath
		reg := regexp.MustCompile(`uploads/`)
		object := reg.ReplaceAllString(v.FilePath, ("/"))

		object, handleErr := s.minio.PutObject(file_path, v.FileType, object)
		if handleErr != nil {
			return handleErr
		}

		storage.MetadataID = v.ID
		storage.Bucket = s.minio.GetBucketName()
		storage.Object = object
		handleErr = s.InsertMinIOStorage(storage)
		if handleErr != nil {
			return handleErr
		}

		handleErr = s.UpdateMetadataIsBackUp(v)
		if handleErr != nil {
			return handleErr
		}
	}

	return nil
}

func unique(data []models.Metadata) []models.Metadata {
	var unique []models.Metadata
uniqueLoop:
	for _, v := range data {
		for i, u := range unique {
			if v.BranchID == u.BranchID && v.FileDate == u.FileDate {
				unique[i] = v
				continue uniqueLoop
			}
		}
		unique = append(unique, v)
	}
	return unique
}

func getExpriredDate() string {
	expired, _ := strconv.Atoi(os.Getenv("EXPIRED_DATE_LOG"))
	expired = -(expired + 1)
	dt := time.Now()
	expired_date := dt.AddDate(0, 0, expired).Format("2006-01-02")
	return expired_date
}
