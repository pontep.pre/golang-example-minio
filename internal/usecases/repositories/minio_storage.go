package repositories

import (
	"app/internal/models"
	"fmt"
	"time"
)

func (r *repository) InsertMinIOStorage(storage models.MinIOStorage) error {
	query := "REPLACE INTO minio_storage (metadata_id , bucket, object, created_at) VALUES (?,?,?,?)"
	row, err := r.sql.Prepare(query)
	defer row.Close()
	if err != nil {
		return err
	}
	unix_time := time.Now().Unix()
	_, err = row.Exec(storage.MetadataID, storage.Bucket, storage.Object, unix_time)
	if err != nil {
		return err
	}
	return nil
}

func (r *repository) GetMinIOStorage(id string) (models.MinIOStorage, error) {
	var data models.MinIOStorage
	query := fmt.Sprintf(`SELECT metadata_id, bucket, object, created_at FROM minio_storage WHERE metadata_id = '%s'`, id)
	row, err := r.sql.Query(query)
	defer row.Close()

	if err != nil {
		return data, err
	}
	for row.Next() {
		err := row.Scan(
			&data.MetadataID,
			&data.Bucket,
			&data.Object,
			&data.CreatedAt,
		)
		if err != nil {
			return data, err
		}
	}

	err = row.Err()
	if err != nil {
		return data, err
	}
	return data, nil
}
