package repositories

import (
	"app/internal/usecases"
	"database/sql"
)

type repository struct {
	sql *sql.DB
}

// NewRepository ...
func NewRepository(sql *sql.DB) usecases.Repository {
	return &repository{sql}
}
