package repositories

import (
	"app/internal/models"
	"fmt"
	"strings"
	"time"
)

func (r *repository) GetMetadata(filters models.FilterMetadata) ([]models.Metadata, error) {
	var data []models.Metadata
	query := getMetadataQuery(filters)
	rows, err := r.sql.Query(query)
	defer rows.Close()
	if err != nil {
		return data, err
	}
	for rows.Next() {
		var metadata models.Metadata
		err := rows.Scan(
			&metadata.ID,
			&metadata.RefID,
			&metadata.BranchID,
			&metadata.Service,
			&metadata.FileType,
			&metadata.FileDate,
			&metadata.FileName,
			&metadata.FileSize,
			&metadata.FilePath,
			&metadata.IsBackup,
			&metadata.IsDelete,
			&metadata.CreatedAt,
			&metadata.UpdatedAt,
		)
		if err != nil {
			return data, err
		}

		data = append(data, metadata)
	}

	err = rows.Err()
	if err != nil {
		return data, err
	}

	return data, nil

}

func (r *repository) GetMetadataByDetail(metadata models.Metadata) (models.Metadata, error) {
	var data models.Metadata
	query := fmt.Sprintf(`SELECT id FROM metadata WHERE branch_id = '%s' AND file_date = '%s' AND file_name = '%s' LIMIT 1;`,
		metadata.BranchID, metadata.FileDate, metadata.FileName)
	row, err := r.sql.Query(query)
	defer row.Close()

	if err != nil {
		return data, err
	}
	for row.Next() {
		err := row.Scan(&data.ID)
		if err != nil {
			return data, err
		}
	}

	err = row.Err()
	if err != nil {
		return data, err
	}
	return data, nil
}

func (r *repository) GetMetadataByID(id string) (models.Metadata, error) {
	var data models.Metadata
	query := fmt.Sprintf(`SELECT 
		id, 
		ref_id, 
		branch_id, 
		service, 
		file_type, 
		file_date, 
		file_name, 
		file_size, 
		file_path, 
		is_backup,
		is_delete, 
		created_at, 
		updated_at 
	FROM metadata 
	WHERE id = '%s'
		OR ref_id = '%s'`, id, id)
	row, err := r.sql.Query(query)
	defer row.Close()

	if err != nil {
		return data, err
	}
	for row.Next() {
		err := row.Scan(
			&data.ID,
			&data.RefID,
			&data.BranchID,
			&data.Service,
			&data.FileType,
			&data.FileDate,
			&data.FileName,
			&data.FileSize,
			&data.FilePath,
			&data.IsBackup,
			&data.IsDelete,
			&data.CreatedAt,
			&data.UpdatedAt,
		)
		if err != nil {
			return data, err
		}
	}

	err = row.Err()
	if err != nil {
		return data, err
	}
	return data, nil
}

func (r *repository) InsertMetadata(metadata models.Metadata) error {
	query := "INSERT INTO metadata (id, ref_id, branch_id, service, file_type, file_date, file_name, file_size, file_path, is_backup, is_delete, created_at, updated_at) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?)"
	row, err := r.sql.Prepare(query)
	defer row.Close()
	if err != nil {
		return err
	}
	unix_time := time.Now().Unix()
	_, err = row.Exec(
		metadata.ID,
		metadata.RefID,
		metadata.BranchID,
		metadata.Service,
		metadata.FileType,
		metadata.FileDate,
		metadata.FileName,
		metadata.FileSize,
		metadata.FilePath,
		0,
		0,
		unix_time,
		0)
	if err != nil {
		return err
	}
	return nil
}

func (r *repository) UpdateMetadata(metadata models.Metadata) error {
	query := fmt.Sprintf(`
	UPDATE metadata 
	SET ref_id=?,
		branch_id=?,
		service=?,
		file_type=?,
		file_date=?,
		file_name=?,
		file_size=?,
		file_path=?,
		updated_at=?
	WHERE id = ?`)
	row, err := r.sql.Prepare(query)
	defer row.Close()
	if err != nil {
		return err
	}
	unix_time := time.Now().Unix()
	_, err = row.Exec(
		metadata.RefID,
		metadata.BranchID,
		metadata.Service,
		metadata.FileType,
		metadata.FileDate,
		metadata.FileName,
		metadata.FileSize,
		metadata.FilePath,
		unix_time,
		metadata.ID,
	)
	if err != nil {
		return err
	}
	return nil
}

func (r *repository) UpdateMetadataIsBackUp(metadata models.Metadata) error {
	query := fmt.Sprintf(`
	UPDATE metadata 
	SET	is_backup=?,
		updated_at=?
	WHERE id = ?`)
	row, err := r.sql.Prepare(query)
	defer row.Close()
	if err != nil {
		return err
	}
	unix_time := time.Now().Unix()
	_, err = row.Exec(
		1,
		unix_time,
		metadata.ID,
	)
	if err != nil {
		return err
	}
	return nil
}

func (r *repository) UpdateMetadataIsDelete(metadata models.Metadata) error {
	query := fmt.Sprintf(`
	UPDATE metadata 
	SET	is_delete=?,
		updated_at=?
	WHERE branch_id = ? AND file_date = ?`)
	row, err := r.sql.Prepare(query)
	defer row.Close()
	if err != nil {
		return err
	}
	unix_time := time.Now().Unix()
	_, err = row.Exec(
		1,
		unix_time,
		metadata.BranchID,
		metadata.FileDate,
	)
	if err != nil {
		return err
	}
	return nil
}

func getMetadataQuery(filters models.FilterMetadata) string {
	query := fmt.Sprintf(`SELECT 
		id, 
		ref_id, 
		branch_id, 
		service, 
		file_type, 
		file_date, 
		file_name, 
		file_size, 
		file_path, 
		is_backup, 
		is_delete,
		created_at, 
		updated_at 
	FROM metadata 
	WHERE service = "d1669"`)

	var condition []string
	if filters.BranchID != "" {
		condition = append(condition, fmt.Sprintf(` AND branch_id '%s'`, filters.BranchID))
	}

	if filters.FileDate != "" {
		condition = append(condition, fmt.Sprintf(` AND file_date = '%s'`, filters.FileDate))
	}

	if filters.FileName != "" {
		condition = append(condition, fmt.Sprintf(` AND file_name = '%s'`, filters.FileName))
	}

	if filters.FileType != "" {
		condition = append(condition, fmt.Sprintf(` AND file_type = '%s'`, filters.FileType))
	}

	if filters.IsBackup != "" {
		condition = append(condition, fmt.Sprintf(` AND is_backup = '%s'`, filters.IsBackup))
	}

	if filters.IsDelete != "" {
		condition = append(condition, fmt.Sprintf(` AND is_delete = '%s'`, filters.IsDelete))
	}

	if len(condition) > 0 {
		str_condition := strings.Join(condition, "")
		query = (query + str_condition)
	}

	return query
}
