package models

type MinIOStorage struct {
	MetadataID string `json:"metadata_id"`
	Bucket     string `json:"bucket"`
	Object     string `json:"object"`
	CreatedAt  int    `json:"created_at"`
}
