package models

type Metadata struct {
	ID        string `json:"id"`
	RefID     string `json:"ref_id"`
	BranchID  string `json:"branch_id"`
	Service   string `json:"service"`
	FileType  string `json:"file_type"`
	FileDate  string `json:"file_date"`
	FileName  string `json:"file_name"`
	FileSize  string `json:"file_size"`
	FilePath  string `json:"file_path"`
	IsBackup  string `json:"is_backup"`
	IsDelete  string `json:"is_delete"`
	CreatedAt int    `json:"created_at"`
	UpdatedAt int    `json:"updated_at"`
}

type ResponseMetadata struct {
	ID       string `json:"id"`
	FileName string `json:"file_name"`
}

type FilterMetadata struct {
	BranchID string `json:"branch_id"`
	FileType string `json:"file_type"`
	FileDate string `json:"file_date"`
	FileName string `json:"file_name"`
	IsBackup string `json:"is_backup"`
	IsDelete string `json:"is_delete"`
}
