package hash

import (
	"crypto/sha1"
	"fmt"
)

func EncryptSHA1Hash(data []byte) string {
	hash := fmt.Sprintf("%x", sha1.Sum(data))
	return hash
}
