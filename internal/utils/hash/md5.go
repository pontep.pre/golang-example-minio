package hash

import (
	"crypto/md5"
	"fmt"
)

func EncryptMD5Hash(data []byte) string {
	hash := fmt.Sprintf("%x", md5.Sum(data))
	return hash
}
