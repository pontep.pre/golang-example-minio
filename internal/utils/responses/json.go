package responses

import (
	"encoding/json"
	"net/http"
)

type Response interface {
	JsonOk(w http.ResponseWriter, code int, message string)
	JsonFail(w http.ResponseWriter, code int, message string)
	JsonInterface(w http.ResponseWriter, code int, payload interface{})
}

type httpJson struct{}

func NewResponse() Response {
	return &httpJson{}
}

func (r *httpJson) JsonInterface(w http.ResponseWriter, code int, payload interface{}) {
	WriteResponse(w, code, payload)
}

func (r *httpJson) JsonOk(w http.ResponseWriter, code int, message string) {
	payload := map[string]string{"status": "OK", "message": message}
	WriteResponse(w, code, payload)
}

func (r *httpJson) JsonFail(w http.ResponseWriter, code int, message string) {
	payload := map[string]string{"status": "FAIL", "message": message}
	WriteResponse(w, code, payload)
}

func WriteResponse(w http.ResponseWriter, code int, payload interface{}) {
	response, err := json.Marshal(payload)
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		w.Write([]byte(err.Error()))
		return
	}
	w.Header().Set("Content-Type", "application/json")
	w.WriteHeader(code)
	w.Write([]byte(response))
}
