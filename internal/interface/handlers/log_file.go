package handlers

import (
	"app/internal/models"
	"app/internal/utils/convert"
	"fmt"
	"net/http"
	"os"
	"strconv"

	"github.com/google/uuid"
	"github.com/gorilla/mux"
)

func (h *handler) InsertLogFile(w http.ResponseWriter, r *http.Request) {
	var data []models.ResponseMetadata
	var metadata models.Metadata
	metadata.BranchID = r.FormValue("branch_id")
	metadata.RefID = r.FormValue("ref_id")
	metadata.FileDate = r.FormValue("file_date")

	max_upload_siz_of_mb, _ := strconv.Atoi(os.Getenv("MAX_UPLOAD_SIZE"))
	max_upload_size := int64(max_upload_siz_of_mb * 1024 * 1024)
	if err := r.ParseMultipartForm(max_upload_size); err != nil {
		msg_err := fmt.Sprintf(`upload_file_fail: %s`, err)
		h.logger.LogEntry().Error(msg_err)
		h.response.JsonFail(w, http.StatusBadRequest, msg_err)
		return
	}

	files := r.MultipartForm.File["files"]
	if len(files) == 0 {
		msg_err := fmt.Sprintf(`Please select files`)
		h.response.JsonFail(w, http.StatusBadRequest, msg_err)
		return
	}

	for _, fileHeader := range files {
		if fileHeader.Size > max_upload_size {
			msg_err := fmt.Sprintf(`The uploaded file is to big "%s". Please use an files less than %v MB in size`, fileHeader.Filename, max_upload_siz_of_mb)
			h.logger.LogEntry().Error(msg_err)
			h.response.JsonFail(w, http.StatusBadRequest, msg_err)
			return
		}

		file, err := fileHeader.Open()
		defer file.Close()
		if err != nil {
			msg_err := fmt.Sprintf(`upload_file_fail: %s`, err)
			h.logger.LogEntry().Error(msg_err)
			h.response.JsonFail(w, http.StatusBadRequest, msg_err)
			return
		}

		metadata.Service = "d1669"
		metadata.FileName = fileHeader.Filename
		metadata.FileSize = convert.ByteSize(int(fileHeader.Size), 2)
		metadata.FileType, err = h.service.GetFileType(file)
		if err != nil {
			msg_err := fmt.Sprintf(`upload_file_fail: %s`, err)
			h.logger.LogEntry().Error(msg_err)
			h.response.JsonFail(w, http.StatusInternalServerError, msg_err)
			return
		}

		path := metadata.BranchID + "/" + metadata.FileDate + "/" + metadata.RefID + "/"
		asset, err := h.service.UploadFile(file, metadata.FileName, path)
		if err != nil {
			msg_err := fmt.Sprintf(`upload_file_fail: %s`, err)
			h.logger.LogEntry().Error(msg_err)
			h.response.JsonFail(w, http.StatusInternalServerError, msg_err)
			return
		}
		metadata.FilePath = asset

		log_file, err := h.service.GetMetadataByDetail(metadata)
		if err != nil {
			msg_err := fmt.Sprintf(`upload_file_fail: %s`, err)
			h.logger.LogEntry().Error(msg_err)
			h.response.JsonFail(w, http.StatusInternalServerError, msg_err)
			return
		}

		if log_file.ID != "" {
			metadata.ID = log_file.ID
			err = h.service.UpdateMetadata(metadata)
			if err != nil {
				msg_err := fmt.Sprintf(`upload_file_fail: %s`, err)
				h.logger.LogEntry().Error(msg_err)
				h.response.JsonFail(w, http.StatusInternalServerError, msg_err)
				return
			}
		} else {
			metadata.ID = uuid.NewString()
			err = h.service.InsertMetadata(metadata)
			if err != nil {
				msg_err := fmt.Sprintf(`upload_file_fail: %s`, err)
				h.logger.LogEntry().Error(msg_err)
				h.response.JsonFail(w, http.StatusInternalServerError, msg_err)
				return
			}
		}

		var result models.ResponseMetadata
		result.ID = metadata.ID
		result.FileName = metadata.FileName
		data = append(data, result)
	}

	h.response.JsonInterface(w, http.StatusCreated, map[string]interface{}{
		"status":  "OK",
		"message": "uploaded_success",
		"data":    data,
	})
}

func (h *handler) GetPresignUrl(w http.ResponseWriter, r *http.Request) {
	vars := mux.Vars(r)
	id := vars["id"]

	result, err := h.service.GetMetadataByID(id)
	if err != nil {
		msg_err := fmt.Sprintf(`execute database fail: %s`, err)
		h.logger.LogEntry().Error(msg_err)
		h.response.JsonFail(w, http.StatusInternalServerError, msg_err)
		return
	}

	if result.ID == "" {
		h.response.JsonFail(w, http.StatusNotFound, "logfile_not_found")
		return
	}

	var url string
	if result.IsBackup == "1" {
		url, err = h.service.GetPresignUrl(result)
		if err != nil {
			msg_err := fmt.Sprintf(`generate url fail: %s`, err)
			h.logger.LogEntry().Error(msg_err)
			h.response.JsonFail(w, http.StatusInternalServerError, msg_err)
			return
		}
	} else {
		url = r.Host + h.service.GetAssetFile(result.FilePath)
	}

	h.response.JsonInterface(w, http.StatusOK, map[string]interface{}{
		"status":  "OK",
		"message": "generate_url_success",
		"data": map[string]interface{}{
			"url": url,
		},
	})
}

// func (h *handler) DeleteLogFile(w http.ResponseWriter, r *http.Request) {
// 	err := h.service.DeleteLogFile()
// 	if err != nil {
// 		msg_err := fmt.Sprintf(`delete file fail: %s`, err)
// 		h.logger.LogEntry().Error(msg_err)
// 		h.response.JsonFail(w, http.StatusInternalServerError, msg_err)
// 		return
// 	}

// 	h.response.JsonOk(w, http.StatusOK, "delete_file_success")
// }

// func (h *handler) BackupLogFile(w http.ResponseWriter, r *http.Request) {
// 	err := h.service.BackupLogFile()
// 	if err != nil {
// 		msg_err := fmt.Sprintf(`backup file fail: %s`, err)
// 		h.logger.LogEntry().Error(msg_err)
// 		h.response.JsonFail(w, http.StatusInternalServerError, msg_err)
// 		return
// 	}

// 	h.response.JsonOk(w, http.StatusOK, "backup_file_success")
// }
