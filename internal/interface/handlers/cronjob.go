package handlers

import (
	"fmt"
)

func (h *handler) JobBackupLogFile() {
	err := h.service.BackupLogFile()
	if err != nil {
		msg_err := fmt.Sprintf(`backup file fail: %s`, err)
		h.logger.LogEntry().Error(msg_err)
		return
	}

	h.logger.LogEntry().Info("backup file success")
}

func (h *handler) JobDeleteLogFile() {
	err := h.service.DeleteLogFile()
	if err != nil {
		msg_err := fmt.Sprintf(`delete file fail: %s`, err)
		h.logger.LogEntry().Error(msg_err)
		return
	}

	h.logger.LogEntry().Info("delete file success")
}
