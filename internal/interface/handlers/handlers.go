package handlers

import (
	"app/internal/usecases"
	"app/internal/utils/responses"
	"app/pkg/logs"
	"fmt"
	"net/http"
	"os"
)

type handler struct {
	service  usecases.Service
	response responses.Response
	logger   logs.Logrus
}

// NewHandler ...
func NewHandler(response responses.Response, logger logs.Logrus, service usecases.Service) *handler {
	return &handler{
		service:  service,
		response: response,
		logger:   logger,
	}
}

func (h *handler) GetHelpCheck(w http.ResponseWriter, r *http.Request) {
	message := fmt.Sprintf(`[√] Service api-to-logsfile version %s started`, os.Getenv("API_VERSION"))
	h.logger.LogEntry().Info(message)
	h.response.JsonOk(w, 200, message)
}
