package routes

import (
	"app/internal/interface/handlers"
	"app/internal/usecases/repositories"
	"app/internal/usecases/services"
	"app/internal/utils/responses"
	"app/pkg/databases"
	"app/pkg/logs"
	"net/http"

	"github.com/gorilla/mux"
	"github.com/mileusna/crontab"
)

func SetupRouter() *mux.Router {
	repositories := repositories.NewRepository(databases.DB)
	services := services.NewService(repositories)
	response := responses.NewResponse()
	logger := logs.NewLogrus()
	handlers := handlers.NewHandler(response, logger, services)

	//Crontab Backup  and Delete expire log file
	ctab := crontab.New()
	err := ctab.AddJob("* 1 * * *", handlers.JobBackupLogFile)
	if err != nil {
		logger.Error("crontab backup file error:", err)
	}
	err = ctab.AddJob("* 2 * * *", handlers.JobDeleteLogFile)
	if err != nil {
		logger.Error("crontab delete file error:", err)
	}

	r := mux.NewRouter()
	r.HandleFunc("/", handlers.GetHelpCheck).Methods("GET")
	r.HandleFunc("/v1/logs", handlers.InsertLogFile).Methods("POST")
	r.HandleFunc("/v1/logs/{id}/presign-url", handlers.GetPresignUrl).Methods("GET")
	//r.HandleFunc("/v1/logs/backup", handlers.BackupLogFile).Methods("GET")
	//r.HandleFunc("/v1/logs", handlers.DeleteLogFile).Methods("DELETE")
	//route staticfile
	r.PathPrefix("/files/").Handler(http.StripPrefix("/files/", http.FileServer(http.Dir("uploads/"))))
	//r.Use(apmgorilla.Middleware())
	return r
}
