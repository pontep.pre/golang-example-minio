package main

import (
	"app/internal/interface/routes"
	"app/pkg/databases"
	"app/pkg/logs"
	"app/pkg/storages"
	"fmt"
	"net/http"
	"os"

	"github.com/rs/cors"
)

var logger = logs.NewLogrus()

func main() {
	defer panicHandler()
	db, err := databases.NewMysql(os.Getenv("DB_HOST"))
	if err != nil {
		panic("NotConnectException")
	}
	defer db.Close()

	_, err = storages.NewMinIO()
	if err != nil {
		panic("NotConnectStorage")
	}

	r := routes.SetupRouter()
	cors := setCors()
	message := fmt.Sprintf(`ListenAndServe api-to-logsfile version %s started`, os.Getenv("API_VERSION"))
	logger.LogEntry().Info(message)
	err = http.ListenAndServe(":8080", cors.Handler(r))
	if err != nil {
		panic("NotServeException")
	}
}

func panicHandler() {
	err := recover()
	if err == "NotConnectException" {
		logger.LogEntry().Fatal("Cannot connect mysql, Program terminated.")
	}
	if err == "NotConnectStorage" {
		logger.LogEntry().Fatal("Cannot connect minio server, Program terminated.")
	}
	if err == "NotConnectDBException" {
		logger.LogEntry().Fatal("Cannot connect database, Program terminated.")
	}
	if err == "NotServeException" {
		logger.LogEntry().Fatal("Cannot listen and serve, Program terminated.")
	}
}

func setCors() *cors.Cors {
	cors := cors.New(cors.Options{
		AllowedOrigins: []string{"*"}, //you service is available and allowed for this base url
		AllowedMethods: []string{
			http.MethodGet, //http methods for your app
			http.MethodPost,
			http.MethodPut,
			http.MethodDelete,
		},
		AllowedHeaders: []string{
			"*", //or you can your header key values which you are using in your application
		},
	})
	return cors
}
